FROM python:3

RUN mkdir -p /project
ADD requirements.txt /project
COPY src /project/.

RUN pip install -r /project/requirements.txt
WORKDIR /project/rock_n_iasi_tracking

RUN python manage.py migrate
ENTRYPOINT python manage.py runserver 0.0.0.0:8000
